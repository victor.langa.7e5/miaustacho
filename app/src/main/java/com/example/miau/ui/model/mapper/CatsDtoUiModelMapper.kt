package com.example.miau.ui.model.mapper

import com.example.miau.data.apiservice.model.CatsDto
import com.example.miau.data.apiservice.model.CatsImagesDto
import com.example.miau.ui.model.CatsUiModel

class CatsDtoUiModelMapper {
    fun map(catsDto: List<CatsDto>, catsImagesDto: List<CatsImagesDto?>): List<CatsUiModel> {
        return mutableListOf<CatsUiModel>().apply {
            for (i in catsDto.indices)
                add(
                    CatsUiModel(
                        catsDto[i].id, catsDto[i].name, catsDto[i].temperament,
                        catsDto[i].countryCode, catsDto[i].description, catsDto[i].wikipedia_url,
                        catsImagesDto?.get(i)?.imageUrl ?: ""
                    )
                )
        }
    }
}