package com.example.miau.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CatsImagesDto(
    @SerialName("url") val imageUrl: String = ""
)