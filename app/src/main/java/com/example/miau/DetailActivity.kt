package com.example.miau

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.miau.ui.theme.MiauTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import androidx.compose.foundation.background

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MiauTheme {
                intent.getStringArrayExtra("catsUiModel")?.let { MiauDetail(it) }
            }
        }
    }

    @Composable
    fun MiauDetail(catsUiModel: Array<String>) {

        val uriHandler = LocalUriHandler.current

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxSize()
                .background(color = Color.DarkGray)
                .verticalScroll(rememberScrollState())
        ) {
            GlideImage(
                imageModel = { catsUiModel[6] },
                imageOptions = ImageOptions(contentScale = ContentScale.Crop),
                modifier = Modifier
                    .size(350.dp)
                    .padding(35.dp)
            )
            Text(
                text = catsUiModel[1],
                fontFamily = FontFamily.Monospace,
                fontSize = 25.sp,
                fontWeight = FontWeight.ExtraBold,
                modifier = Modifier.padding(bottom = 50.dp, start = 25.dp, end = 25.dp),
                color = Color.White
            )
            Text(
                text = catsUiModel[4],
                modifier = Modifier.padding(start = 10.dp, end = 25.dp, bottom = 50.dp),
                textAlign = TextAlign.Justify,
                color = Color.White
            )

            Text(
                text = catsUiModel[2],
                textAlign = TextAlign.Justify,
                modifier = Modifier.padding(10.dp),
                color = Color.White
            )
            Text(
                text = "Country code: " + catsUiModel[3],
                textAlign = TextAlign.Justify,
                modifier = Modifier.padding(10.dp),
                color = Color.White
            )

            Text(
                text = catsUiModel[5],
                modifier = Modifier
                    .padding(start = 25.dp, end = 25.dp, bottom = 50.dp)
                    .clickable { uriHandler.openUri(catsUiModel[5]) },
                textAlign = TextAlign.Justify,
                color = Color.Cyan,
                textDecoration = TextDecoration.Underline
            )
        }
    }
}